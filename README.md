# Assets 👾

Hello there! You've arrived at my **Assets** repository. This is where I store any resources, such as images and GIFs, that are used in the `README.md` files of my GitLab projects.

> Usually, GitLab projects include these assets in the `main` branch of their respective repositories. However, to prevent unnecessarily increasing the `git clone` size for those who clone my projects, I maintain this separate repository to centralize all the assets for my personal repositories.